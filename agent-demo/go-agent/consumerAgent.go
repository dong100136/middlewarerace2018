package main

import (
	"encoding/binary"
	"fmt"
	"log"
	"net"
	"net/http"
	"reflect"
	"unsafe"
)

func index(w http.ResponseWriter, r *http.Request) {
	var p = r.PostFormValue("parameter")
	channel := make(chan int32)
	go func(msg string) {
		// time.Sleep(50 * time.Millisecond)
		conn, err := net.Dial("tcp", "127.0.0.1:30000")
		rpcResqest := &RpcResqest.RpcRequest{}
		channel <- hashCode(msg) // 存消息
	}(p)
	fmt.Fprint(w, <-channel)
}

func hashCode(s string) int32 {
	var h int32 = 0
	b := stringtoslicebyte(s)
	for i := 0; i < len(b); i++ {
		h = 31*h + int32(b[i])
	}

	return h
}

func Int64ToBytes(i int64) []byte {
	var buf = make([]byte, 8)
	binary.BigEndian.PutUint64(buf, uint64(i))
	return buf
}

func stringtoslicebyte(s string) []byte {
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	bh := reflect.SliceHeader{
		Data: sh.Data,
		Len:  sh.Len,
		Cap:  sh.Len,
	}
	return *(*[]byte)(unsafe.Pointer(&bh))
}

func main() {
	// 设置路由，如果访问/，则调用index方法
	http.HandleFunc("/", index)

	// 启动web服务，监听9090端口
	err := http.ListenAndServe(":20000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}
