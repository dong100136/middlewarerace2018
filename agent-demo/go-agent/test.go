package main

import "fmt"
import "encoding/binary"

func main() {
	buf := make([]byte, 8)
	binary.BigEndian.PutUint64(buf, uint64(1000))
	fmt.Println(buf)
}
