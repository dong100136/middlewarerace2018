package com.alibaba.dubbo.performance.demo.agent;

import com.alibaba.dubbo.performance.demo.agent.agentRpcServer.AgentRpcServer;
import com.alibaba.dubbo.performance.demo.agent.httpServer.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgentApp {
    // agent会作为sidecar，部署在每一个Provider和Consumer机器上
    // 在Provider端启动agent时，添加JVM参数-Dtype=provider -Dserver.port=30000 -Ddubbo.protocol.port=20889
    // 在Consumer端启动agent时，添加JVM参数-Dtype=consumer -Dserver.port=20000
    // 添加日志保存目录: -Dlogs.dir=/path/to/your/logs/dir。请安装自己的环境来设置日志目录。

    Logger logger = LoggerFactory.getLogger(AgentApp.class);

    public static void main(String[] args) throws Exception {
        String type = System.getProperty("type");   // 获取type参数
        if ("consumer".equals(type)) {
            new HttpServer().start(Integer.valueOf(System.getProperty("server.port")));
        } else if ("provider".equals(type)) {
            String[] ports = System.getProperty("server.port").split(",");
            new AgentRpcServer(ports).start();
        }
    }
}
