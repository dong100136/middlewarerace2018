package com.alibaba.dubbo.performance.demo.agent;

import com.alibaba.dubbo.performance.demo.agent.codec.JsonUtils;
import com.alibaba.dubbo.performance.demo.agent.model.Request;
import com.alibaba.dubbo.performance.demo.agent.model.RpcInvocation;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.netty.handler.codec.http.multipart.MixedAttribute;
import io.netty.util.CharsetUtil;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class Utils {
    private  static AtomicLong idHolder = new AtomicLong();

    public static long getRequestId(){
        return idHolder.getAndIncrement();
    }

    public final static Request getRequest(long requestId,RpcInvocation invocation) {
        Request request = new Request();
        request.setId(requestId);
        request.setVersion("2.0.0");
        request.setTwoWay(true);
        request.setData(invocation);

        return request;
    }

    public final static RpcInvocation getRpcInvocation(String interfaceName, String method,
                                                 String parameterTypesString, String parameter) throws IOException {
        RpcInvocation invocation = new RpcInvocation();
        invocation.setMethodName(method);
        invocation.setAttachment("path", interfaceName);
        invocation.setParameterTypes(parameterTypesString);    // Dubbo内部用"Ljava/lang/String"来表示参数类型是String

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
        JsonUtils.writeObject(parameter, writer);
        invocation.setArguments(out.toByteArray());

        return invocation;
    }


    public final static HttpResponse getHttpResponse(byte[] data) {
        // 构造FullHttpResponse对象，FullHttpResponse包含message body
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, Unpooled.wrappedBuffer(data));
        response.headers().set("Content-Type", "text/html; charset=utf-8");
        response.headers().set("Content-Length", data.length);
        return response;
    }

//    public final static Map<String, String> parsePostData(HttpRequest request) throws IOException {
//        HashMap<String, String> parameters = new HashMap<>();
//        HttpPostRequestDecoder postData = new HttpPostRequestDecoder(request);
//        List<InterfaceHttpData> datas = postData.getBodyHttpDatas();
//        for (InterfaceHttpData data : datas) {
//            MixedAttribute mixedAttribute = (MixedAttribute) data;
//            mixedAttribute.setCharset(CharsetUtil.UTF_8);
//            String key = mixedAttribute.getName();
//            String value = mixedAttribute.getValue();
//            parameters.put(key, value);
//        }
//
//        return parameters;
//    }
}
