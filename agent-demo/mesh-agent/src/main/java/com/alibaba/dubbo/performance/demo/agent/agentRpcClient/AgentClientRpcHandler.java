package com.alibaba.dubbo.performance.demo.agent.agentRpcClient;

import com.alibaba.dubbo.performance.demo.agent.model.RpcResponse;
import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcResponseProto;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class AgentClientRpcHandler extends SimpleChannelInboundHandler<RpcResponseProto.RpcResponse> {

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcResponseProto.RpcResponse msg) throws Exception {
        AgentRpcClient.handleRpcResponse(ctx,msg);
    }
}
