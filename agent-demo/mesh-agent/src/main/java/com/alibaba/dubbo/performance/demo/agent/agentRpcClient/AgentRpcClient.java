package com.alibaba.dubbo.performance.demo.agent.agentRpcClient;

import com.alibaba.dubbo.performance.demo.agent.Utils;
import com.alibaba.dubbo.performance.demo.agent.model.ObjectHolder;
import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcRequestProto;
import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcResponseProto;
import com.alibaba.dubbo.performance.demo.agent.monitor.AgentMonitor;
import com.alibaba.dubbo.performance.demo.agent.registry.ChannelMgr;
import com.alibaba.dubbo.performance.demo.agent.registry.Endpoint;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class AgentRpcClient {
    static List<ObjectHolder<ChannelHandlerContext>> httpChannelHolders = new ArrayList<>();
    private static Logger logger = LoggerFactory.getLogger(AgentRpcClient.class);
    private static AgentRpcClient agentRpcClient;
    private static Random random = new Random();
    private static final long REQUEST_MASK = 3;
    private Bootstrap bootstrap;
    private NioEventLoopGroup eventLoopGroup;
    private Map<String, Channel> channels = new ConcurrentHashMap<>();
    private Object lock = new Object();

    public static AgentRpcClient getInstance() {
        if (agentRpcClient == null) {
            agentRpcClient = new AgentRpcClient();
            agentRpcClient.initBootstrap();
            for(int i = 0; i <= REQUEST_MASK; i++) {
                httpChannelHolders.add(new ObjectHolder<>());
            }
        }
        return agentRpcClient;
    }

    public static void invoke(ChannelHandlerContext ctx, String interfaceName, String method, String parameterTypesString,
                              String parameter) throws Exception {
//        负载均衡
        Channel channel = getInstance().getChannel();
        RpcRequestProto.RpcRequest.Builder requestBuilder = RpcRequestProto.RpcRequest.newBuilder();
        long requestId = Utils.getRequestId();
        requestBuilder.setRequestId(requestId);
        requestBuilder.setParameter(parameter);
        requestBuilder.setHc(parameter.hashCode());
        requestBuilder.setCreateTime(System.currentTimeMillis());
        requestBuilder.setReqBeginTime(System.currentTimeMillis());

        channel.writeAndFlush(requestBuilder.build());

        int holderId = (int) (REQUEST_MASK & requestId);
        ObjectHolder<ChannelHandlerContext> holder = httpChannelHolders.get(holderId);
        holder.put(requestId, ctx);
        logger.info(String.format("agent client invoke requestId = %d, go to (%d, %d)", requestId, holderId,
                holder.count()));
    }

    public static void handleRpcResponse(ChannelHandlerContext ctx, RpcResponseProto.RpcResponse response) {
        Long requestId = response.getRequestId();
        HttpResponse rs = Utils.getHttpResponse(String.valueOf(response.getHashcode()).getBytes());
        ObjectHolder<ChannelHandlerContext> holder = httpChannelHolders.get((int) (REQUEST_MASK & requestId));
        ChannelHandlerContext ctx2 = holder.get(requestId);
        if (ctx2 != null) {
            ctx2.writeAndFlush(rs);
        }

        holder.remove(requestId);
        logger.info(String.format("agent client get response requestId = %d", requestId));

        if (response.getHc() != response.getHashcode()) {
            System.out.println(String.format("agent client get error response requestId = %d \n\n origin code : %d \n get hashcode : %d \n",
                    requestId, response.getHc(), response.getHashcode()));
        }


        long now = System.currentTimeMillis();
        String hostname = ctx.channel().remoteAddress().toString().split("/")[1].trim();
        ChannelMgr.update(hostname, now - response.getCreateTime());
        ChannelMgr.active(hostname,now);

//        测试性能的代码
        AgentMonitor.getInstance().addChannelRequest(hostname,
                now - response.getCreateTime(),
                response.getReqEndTime() - response.getReqBeginTime(),
                now - response.getRespBeginTime());
        AgentMonitor.getInstance().addRequest(now - response.getCreateTime(),
                response.getReqEndTime() - response.getReqBeginTime(),
                now - response.getRespBeginTime());
    }

    public Channel getChannel() throws Exception {
        Endpoint endpoint = ChannelMgr.getServerByRandom();
        String hostName = endpoint.getHost();
        int port = endpoint.getPort();

        String channelName = hostName + ":" + port + ":" + random.nextInt(10);
//        String channelName = hostName + ":" + port;
        Channel channel = channels.get(channelName);

        if (null == channel) {
            synchronized (lock) {
                if (null == channel) {
                    channel = bootstrap.connect(hostName, port).sync().channel();
                    channels.put(channelName, channel);
                }
            }
        }

        return channel;
    }

    private void initBootstrap() {
        eventLoopGroup = new NioEventLoopGroup(4);
        bootstrap = new Bootstrap()
                .group(eventLoopGroup)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .option(ChannelOption.TCP_NODELAY, true)
                .option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .channel(NioSocketChannel.class)
                .handler(new ChannelHandlerInitializer());
    }
}


