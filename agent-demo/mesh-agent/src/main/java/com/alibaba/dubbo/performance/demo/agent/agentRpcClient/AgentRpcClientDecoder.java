package com.alibaba.dubbo.performance.demo.agent.agentRpcClient;

import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcResponseProto;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.io.InputStream;
import java.util.List;

public class AgentRpcClientDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list)
            throws Exception {
        InputStream in = new ByteBufInputStream(byteBuf);
        list.add(RpcResponseProto.RpcResponse.parseFrom(in));
    }
}
