package com.alibaba.dubbo.performance.demo.agent.agentRpcClient;

import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcRequestProto;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;

public class AgentRpcClientEncoder extends MessageToByteEncoder {

    private static Logger logger = LoggerFactory.getLogger(AgentRpcClientEncoder.class);

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object msg, ByteBuf byteBuf) throws Exception {
        //logger.info(byteBuf.alloc() + ": " + byteBuf.alloc().isDirectBufferPooled());
        RpcRequestProto.RpcRequest request = (RpcRequestProto.RpcRequest) msg;
        OutputStream out = new ByteBufOutputStream(byteBuf);
        request.writeTo(out);
    }
}
