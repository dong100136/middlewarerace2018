package com.alibaba.dubbo.performance.demo.agent.agentRpcClient;

import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcResponseProto;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;

public class ChannelHandlerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new ProtobufVarint32FrameDecoder());
        //pipeline.addLast(new ProtobufDecoder(RpcResponseProto.RpcResponse.getDefaultInstance()));
        pipeline.addLast(new AgentRpcClientDecoder());
        pipeline.addLast(new ProtobufVarint32LengthFieldPrepender());
        //pipeline.addLast(new ProtobufEncoder());
        pipeline.addLast(new AgentRpcClientEncoder());
        pipeline.addLast(new AgentClientRpcHandler());
    }
}
