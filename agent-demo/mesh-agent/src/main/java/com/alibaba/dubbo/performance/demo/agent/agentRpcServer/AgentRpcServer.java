package com.alibaba.dubbo.performance.demo.agent.agentRpcServer;

import com.alibaba.dubbo.performance.demo.agent.dubboClient.DubboClient;
import com.alibaba.dubbo.performance.demo.agent.model.Bytes;
import com.alibaba.dubbo.performance.demo.agent.model.ObjectHolder;
import com.alibaba.dubbo.performance.demo.agent.model.RpcRequest;
import com.alibaba.dubbo.performance.demo.agent.model.RpcResponse;
import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcRequestProto;
import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcResponseProto;
import com.alibaba.dubbo.performance.demo.agent.registry.EtcdRegistry;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AgentRpcServer {
    private final String[] ports;
    private static Logger logger = LoggerFactory.getLogger(AgentRpcServer.class);
    private EtcdRegistry etcdRegistry = new EtcdRegistry(System.getProperty("etcd.url"));
    private static ObjectHolder<RpcRequestProto.RpcRequest> requestObjectHolder = new ObjectHolder<>();

    private static ExecutorService executorService = Executors.newCachedThreadPool();

    public AgentRpcServer(String[] ports) {
        this.ports = ports;
    }

    public void start() throws Exception {
        EventLoopGroup group = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(group, workerGroup).channel(NioServerSocketChannel.class)
//                    .localAddress(new InetSocketAddress(port))
                    .childHandler(new AgentRpcServerInitializer())
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
            for (int i = 0;i<ports.length;i++) {
                ChannelFuture f = b.bind(Integer.parseInt(ports[i])).sync();
                f.channel().closeFuture().sync();
            }
        } finally {
            group.shutdownGracefully().sync();
            workerGroup.shutdownGracefully().sync();
        }
    }

    public static void handleRequest(ChannelHandlerContext ctx, RpcRequestProto.RpcRequest msg) throws Exception {
        RpcRequestProto.RpcRequest.Builder builder = msg.toBuilder();
        builder.setReqEndTime(System.currentTimeMillis());
        requestObjectHolder.put(msg.getRequestId(),builder.build());

        DubboClient.invoke(ctx,msg.getRequestId(),msg.getParameter());

        /*executorService.submit(() -> {
            try {
                Thread.sleep(50);
                handleResponse(ctx,msg.getRequestId(),String.valueOf(msg.getParameter().hashCode()).getBytes());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });*/
        logger.info(String.format("agent server get request requestId = %d", msg.getRequestId()));
    }


    public static void handleResponse(ChannelHandlerContext ctx,long requestId,byte[] hashcode){
        RpcRequestProto.RpcRequest request = requestObjectHolder.get(requestId);
        RpcResponseProto.RpcResponse.Builder builder = RpcResponseProto.RpcResponse.newBuilder();
        builder.setRequestId(requestId);
        builder.setCreateTime(request.getCreateTime());
        builder.setReqBeginTime(request.getReqBeginTime());
        builder.setReqEndTime(request.getReqEndTime());
        builder.setRespBeginTime(System.currentTimeMillis());
        int hc = Integer.parseInt(new String(hashcode).trim());
        builder.setHashcode(hc);
        builder.setHc(request.getHc());

        ctx.writeAndFlush(builder.build());

        logger.info(String.format("agent server handle request requestId = %d", request.getRequestId()));

        if (request.getHc() != Long.parseLong(new String(hashcode).trim())) {
            logger.error(String.format("agent client get error response requestId = %d \n\n origin code : %d \n get hashcode : %d \n",
                    requestId,request.getHc(), hc));
        }
    }
}

