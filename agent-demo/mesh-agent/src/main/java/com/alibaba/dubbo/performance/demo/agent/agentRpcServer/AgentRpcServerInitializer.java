package com.alibaba.dubbo.performance.demo.agent.agentRpcServer;

import com.alibaba.dubbo.performance.demo.agent.codec.DirectDecode;
import com.alibaba.dubbo.performance.demo.agent.codec.DirectEncode;
import com.alibaba.dubbo.performance.demo.agent.codec.LengthFieldEncode;
import com.alibaba.dubbo.performance.demo.agent.model.RpcRequest;
import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcRequestProto;
import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcResponseProto;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AgentRpcServerInitializer extends ChannelInitializer<SocketChannel> {

    private Logger logger = LoggerFactory.getLogger(AgentRpcServerInitializer.class);

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();
        pipeline.addLast(new ProtobufVarint32FrameDecoder());
        pipeline.addLast(new ProtobufDecoder(RpcRequestProto.RpcRequest.getDefaultInstance()));
        pipeline.addLast(new ProtobufVarint32LengthFieldPrepender());
        pipeline.addLast(new ProtobufEncoder());
        pipeline.addLast(new RpcHandler());
    }
}

