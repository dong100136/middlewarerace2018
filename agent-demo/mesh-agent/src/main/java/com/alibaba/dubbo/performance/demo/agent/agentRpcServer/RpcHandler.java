package com.alibaba.dubbo.performance.demo.agent.agentRpcServer;

import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcRequestProto;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ChannelHandler.Sharable
public class RpcHandler extends SimpleChannelInboundHandler<RpcRequestProto.RpcRequest> {

    private Logger logger = LoggerFactory.getLogger(RpcHandler.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcRequestProto.RpcRequest msg) throws Exception {
        AgentRpcServer.handleRequest(ctx,msg);
    }
}
