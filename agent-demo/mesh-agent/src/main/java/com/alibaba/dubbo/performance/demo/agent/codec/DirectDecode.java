package com.alibaba.dubbo.performance.demo.agent.codec;

import com.alibaba.dubbo.performance.demo.agent.model.Bytes;
import com.alibaba.dubbo.performance.demo.agent.monitor.AgentMonitor;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DirectDecode extends ByteToMessageDecoder {
    private static Logger logger = LoggerFactory.getLogger(DirectDecode.class);

    private final  int HEADER_LENGTH = 16;

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        long start = System.currentTimeMillis();
        if (in.readableBytes()>HEADER_LENGTH) {
            int savedReadIndex = in.readerIndex();

            byte[] msg = new byte[HEADER_LENGTH];
            in.readBytes(msg);

            int length = Bytes.bytes2int(msg, 12);
            if (length>in.readableBytes()){
                in.readerIndex(savedReadIndex);
                return;
            }else {
                in.readerIndex(savedReadIndex);
                msg = new byte[HEADER_LENGTH+length];
                in.readBytes(msg);
                out.add(msg);

                long requestId = Bytes.bytes2long(msg, 4);
                logger.info("stone " + requestId  + ":" + System.currentTimeMillis());
                long delta =System.currentTimeMillis()-start;
                AgentMonitor.getInstance().decodeCount.getAndIncrement();
                AgentMonitor.getInstance().decodeTime.getAndAdd(delta);

            }
        }
    }
}
