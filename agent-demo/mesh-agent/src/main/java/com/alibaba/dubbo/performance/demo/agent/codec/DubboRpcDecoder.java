package com.alibaba.dubbo.performance.demo.agent.codec;

import com.alibaba.dubbo.performance.demo.agent.model.Bytes;
import com.alibaba.dubbo.performance.demo.agent.model.RpcResponse;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class DubboRpcDecoder extends ByteToMessageDecoder {

    // header length.
    protected static final int HEADER_LENGTH = 16;
    protected static final byte FLAG_EVENT = (byte) 0x20;
    private Logger logger = LoggerFactory.getLogger(DubboRpcDecoder.class);

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) {
        if (byteBuf.readableBytes() > HEADER_LENGTH) {
            int savedReadIndex = byteBuf.readerIndex();

            byte[] msg = new byte[HEADER_LENGTH];
            byteBuf.readBytes(msg);

            int length = Bytes.bytes2int(msg, 12);
            if (length > byteBuf.readableBytes()) {
                byteBuf.readerIndex(savedReadIndex);
                return;
            } else {
                byteBuf.readerIndex(savedReadIndex);
                msg = new byte[HEADER_LENGTH + length];

                byteBuf.readBytes(msg);
                list.add(decode2(msg));
            }
        }
    }

    private Object decode2(byte[] data) {
        byte[] subArray = Arrays.copyOfRange(data, HEADER_LENGTH + 1, data.length);

        String s = new String(subArray);

        byte[] requestIdBytes = Arrays.copyOfRange(data, 4, 12);
        long requestId = Bytes.bytes2long(requestIdBytes, 0);


        RpcResponse response = new RpcResponse();
        response.setRequestId(requestId);
        response.setBytes(subArray);
        response.setEvent((data[2] & FLAG_EVENT) == 1);
        return response;
    }
}
