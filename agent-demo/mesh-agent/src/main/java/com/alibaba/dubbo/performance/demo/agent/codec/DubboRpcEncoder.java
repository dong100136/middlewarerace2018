package com.alibaba.dubbo.performance.demo.agent.codec;

import com.alibaba.dubbo.performance.demo.agent.model.Bytes;
import com.alibaba.dubbo.performance.demo.agent.model.Request;
import com.alibaba.dubbo.performance.demo.agent.model.RpcInvocation;
import com.alibaba.dubbo.performance.demo.agent.monitor.AgentMonitor;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class DubboRpcEncoder extends MessageToByteEncoder {
    // header length.
    protected static final int HEADER_LENGTH = 16;
    // magic header.
    protected static final short MAGIC = (short) 0xdabb;
    // message flag.
    protected static final byte FLAG_REQUEST = (byte) 0x80;
    protected static final byte FLAG_TWOWAY = (byte) 0x40;
    protected static final byte FLAG_EVENT = (byte) 0x20;
    private static final Logger logger = LoggerFactory.getLogger(DubboRpcEncoder.class);
    private static final byte[] staticString1 = "\"2.0.1\"\n\"com.alibaba.dubbo.performance.demo.provider.IHelloService\"\nnull\n\"hash\"\n\"Ljava/lang/String;\"\n".getBytes();
    private static final byte[] staticString2 = "{\"path\":\"com.alibaba.dubbo.performance.demo.provider.IHelloService\"}\n".getBytes();

    @Override
    protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf buffer) throws Exception {
        long start = System.currentTimeMillis();

        Request req = (Request) msg;

        // header.
        byte[] header = new byte[HEADER_LENGTH];
        // set magic number.
        Bytes.short2bytes(MAGIC, header);

        // set request and serialization flag.
        header[2] = (byte) (FLAG_REQUEST | 6);

        if (req.isTwoWay()) header[2] |= FLAG_TWOWAY;
        if (req.isEvent()) header[2] |= FLAG_EVENT;

        // set request id.
        Bytes.long2bytes(req.getId(), header, 4);

        // encode request data.
        int savedWriteIndex = buffer.writerIndex();
        buffer.writerIndex(savedWriteIndex + HEADER_LENGTH);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        encodeRequestDataWithStaticString(bos, req.getData());

        int len = bos.size();
        buffer.writeBytes(bos.toByteArray());
        Bytes.int2bytes(len, header, 12);

        // write
        buffer.writerIndex(savedWriteIndex);
        buffer.writeBytes(header); // write header.
        buffer.writerIndex(savedWriteIndex + HEADER_LENGTH + len);


        logger.info("stone " + req.getId() + ":" + System.currentTimeMillis());
        long end = System.currentTimeMillis();
        AgentMonitor.getInstance().encodeCount.getAndIncrement();
        AgentMonitor.getInstance().encodeTime.getAndAdd(end - start);
    }

    public void encodeRequestDataWithStaticString(OutputStream out, Object data) throws Exception {
        RpcInvocation inv = (RpcInvocation) data;
        out.write(staticString1);
        out.write(inv.getArguments());
        out.write(staticString2);
        out.flush();
    }

    public void encodeRequestData(OutputStream out, Object data) throws Exception {
        RpcInvocation inv = (RpcInvocation) data;

        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));

        JsonUtils.writeObject(inv.getAttachment("dubbo", "2.0.1"), writer);
        JsonUtils.writeObject(inv.getAttachment("path"), writer);
        JsonUtils.writeObject(inv.getAttachment("version"), writer);
        JsonUtils.writeObject(inv.getMethodName(), writer);
        JsonUtils.writeObject(inv.getParameterTypes(), writer);

        JsonUtils.writeBytes(inv.getArguments(), writer);
        JsonUtils.writeObject(inv.getAttachments(), writer);
    }

    public void encodeRequestDataWithGson(OutputStream out, Object data) throws IOException {
        RpcInvocation inv = (RpcInvocation) data;

        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));

        GsonUtils.writeObject(inv.getAttachment("dubbo", "2.0.1"), writer);
        GsonUtils.writeObject(inv.getAttachment("path"), writer);
        GsonUtils.writeObject(inv.getAttachment("version"), writer);
        GsonUtils.writeObject(inv.getMethodName(), writer);
        GsonUtils.writeObject(inv.getParameterTypes(), writer);

        GsonUtils.writeBytes(inv.getArguments(), writer);
        GsonUtils.writeObject(inv.getAttachments(), writer);

        writer.flush();
    }
}
