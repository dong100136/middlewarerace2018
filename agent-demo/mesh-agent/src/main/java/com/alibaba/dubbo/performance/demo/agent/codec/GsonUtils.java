package com.alibaba.dubbo.performance.demo.agent.codec;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.PrintWriter;

public class GsonUtils {
    private static Gson gson = new Gson();

    public static void writeObject(Object obj, PrintWriter writer) throws IOException {
//        SerializeWriter out = new SerializeWriter();
//        JsonWriter jsonWriter = new JsonWriter(out);
        gson.toJson(obj,writer);
        writer.flush();
    }

    public static void writeBytes(byte[] b, PrintWriter writer) {
        writer.print(new String(b));
        writer.flush();
    }
}
