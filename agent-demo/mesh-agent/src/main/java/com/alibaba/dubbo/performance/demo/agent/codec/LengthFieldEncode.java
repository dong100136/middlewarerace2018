package com.alibaba.dubbo.performance.demo.agent.codec;

import com.alibaba.dubbo.performance.demo.agent.model.Bytes;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author stone
 */
public class LengthFieldEncode extends MessageToByteEncoder<ByteBuf> {
    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out) throws Exception {
        byte[] header = new byte[4];
        Bytes.int2bytes(msg.readableBytes(),header,0);

        out.writeBytes(header);
        out.writeBytes(msg);
    }
}
