package com.alibaba.dubbo.performance.demo.agent.dubboClient;

import com.alibaba.dubbo.performance.demo.agent.Utils;
import com.alibaba.dubbo.performance.demo.agent.agentRpcServer.AgentRpcServer;
import com.alibaba.dubbo.performance.demo.agent.model.*;
import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcRequestProto;
import com.alibaba.dubbo.performance.demo.agent.monitor.AgentMonitor;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.UnpooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class DubboClient {
    private static DubboClient providerRpcClient;
    private static Object lock = new Object();
    private static ObjectHolder<ChannelHandlerContext> ctxHolder = new ObjectHolder<>();
    private static ObjectHolder<Long> beginTimeHolder = new ObjectHolder<>();
    private Logger logger = LoggerFactory.getLogger(DubboClient.class);
    private Bootstrap bootstrap;
    private static Map<Integer,Channel> channels = new ConcurrentHashMap<>();
    private static Random random = new Random();
//    private Channel channel;

    private DubboClient() {
    }

    public static DubboClient getInstance() {
        synchronized (lock) {
            if (providerRpcClient == null) {
                providerRpcClient = new DubboClient();
                providerRpcClient.initBootstrap();
            }
        }
        return providerRpcClient;
    }

    public static void invoke(ChannelHandlerContext ctx, long requestId,String parameter) throws Exception {
        RpcInvocation rpcInvocation = Utils.getRpcInvocation("","","",parameter);
        Request request = Utils.getRequest(requestId,rpcInvocation);

        Channel channel = getInstance().getChannel();
        channel.writeAndFlush(request);

        ctxHolder.put(requestId, ctx);
    }

    public static void handleResponse(RpcResponse msg) {
        long requestId = msg.getRequestId();

        ChannelHandlerContext rpcCtx = ctxHolder.get(requestId);
        if (rpcCtx != null) {
            AgentRpcServer.handleResponse(rpcCtx,requestId,msg.getBytes());
            ctxHolder.remove(requestId);
        }
    }

    public Channel getChannel() throws Exception {
        int randint = random.nextInt(10);
        Channel channel = channels.get(randint);
        if (null == channel) {
            synchronized (lock) {
                if (null == channel) {
                    int port = Integer.valueOf(System.getProperty("dubbo.protocol.port"));
                    channel = bootstrap.connect("127.0.0.1", port).sync().channel();
                    channels.put(randint,channel);
                }
            }
        }

        return channel;
    }

    public void initBootstrap() {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        bootstrap = new Bootstrap()
                .group(eventLoopGroup)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .option(ChannelOption.TCP_NODELAY, true)
                .option(ChannelOption.ALLOCATOR, UnpooledByteBufAllocator.DEFAULT)
                .channel(NioSocketChannel.class)
                .handler(new DubboClientInitializer());
    }
}
