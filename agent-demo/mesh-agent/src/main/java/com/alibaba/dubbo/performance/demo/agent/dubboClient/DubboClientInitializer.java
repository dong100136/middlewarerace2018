package com.alibaba.dubbo.performance.demo.agent.dubboClient;

import com.alibaba.dubbo.performance.demo.agent.codec.DirectDecode;
import com.alibaba.dubbo.performance.demo.agent.codec.DirectEncode;
import com.alibaba.dubbo.performance.demo.agent.codec.DubboRpcDecoder;
import com.alibaba.dubbo.performance.demo.agent.codec.DubboRpcEncoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class DubboClientInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(io.netty.channel.socket.SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new DubboRpcEncoder());
        ch.pipeline().addLast(new DubboRpcDecoder());
        ch.pipeline().addLast(new DubboHandler());
    }
}
