package com.alibaba.dubbo.performance.demo.agent.dubboClient;

import com.alibaba.dubbo.performance.demo.agent.model.RpcResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;

public class DubboHandler extends SimpleChannelInboundHandler<RpcResponse> {

    @Override
    public void channelRead0(ChannelHandlerContext ctx, RpcResponse msg) {
        if (msg.isEvent()){
            ctx.writeAndFlush(msg);
            return;
        }

        DubboClient.handleResponse(msg);
    }
};
