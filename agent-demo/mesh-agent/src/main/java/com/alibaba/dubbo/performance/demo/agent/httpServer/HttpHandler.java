package com.alibaba.dubbo.performance.demo.agent.httpServer;

import com.alibaba.dubbo.performance.demo.agent.Utils;
import com.alibaba.dubbo.performance.demo.agent.agentRpcClient.AgentRpcClient;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.netty.handler.codec.http.multipart.MixedAttribute;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HttpHandler extends SimpleChannelInboundHandler<Object> {
    static Logger logger = LoggerFactory.getLogger(HttpHandler.class);

//    static ExecutorService mExecutor = Executors.newCachedThreadPool();

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

    private static Map<String, String> parsePostData(HttpRequest request) throws IOException {
        HashMap<String, String> parameters = new HashMap<>();
        HttpPostRequestDecoder postData = new HttpPostRequestDecoder(request);
        List<InterfaceHttpData> datas = postData.getBodyHttpDatas();
        for (InterfaceHttpData data : datas) {
            MixedAttribute mixedAttribute = (MixedAttribute) data;
            mixedAttribute.setCharset(CharsetUtil.UTF_8);
            String key = mixedAttribute.getName();
            String value = mixedAttribute.getValue();
            parameters.put(key, value);
        }

        return parameters;
    }

    private static String parseContent(ByteBuf byteBuf, String key) throws Exception {
        int index = byteBuf.readerIndex();
        if(key.equals(byteBuf.toString(index, key.length(), Charset.defaultCharset()))) {
            byteBuf.skipBytes(key.length() + 1);
            index = byteBuf.readerIndex();
            int length = 0;
            while(byteBuf.isReadable()) {
                byte b = byteBuf.readByte();
                if(b == '&')
                    break;
                length++;
            }
            return byteBuf.toString(index, length, Charset.defaultCharset());
        } else {
            throw new Exception("Could not find key " + key + " in post content.");
        }
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof FullHttpRequest) {
            /*Map<String, String> p = parsePostData((HttpRequest) msg);
            AgentRpcClient.invoke(ctx,
                    p.get("interface"),
                    p.get("method"),
                    p.get("parameterTypesString"),
                    p.get("parameter"));*/
            ByteBuf byteBuf = ((FullHttpRequest) msg).content();
            AgentRpcClient.invoke(ctx,
                    parseContent(byteBuf, "interface"),
                    parseContent(byteBuf, "method"),
                    parseContent(byteBuf, "parameterTypesString"),
                    parseContent(byteBuf, "parameter"));
        }
    }
}

//class InvokeTask implements Runnable {
//    private ChannelHandlerContext ctx;
//    private Map<String,String> p;
//
//    public InvokeTask(ChannelHandlerContext ctx, Map<String,String> p) {
//        this.ctx = ctx;
//        this.p = p;
//    }
//
//    @Override
//    public void run() {
//        try {
//            AgentRpcClient.handleRequest(ctx,
//                    p.get("interface"),
//                    p.get("method"),
//                    p.get("parameterTypesString"),
//                    p.get("parameter"));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}

