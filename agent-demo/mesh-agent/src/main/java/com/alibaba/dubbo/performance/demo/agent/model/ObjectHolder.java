package com.alibaba.dubbo.performance.demo.agent.model;

import java.util.concurrent.ConcurrentHashMap;

public class ObjectHolder<T> {
    private ConcurrentHashMap<Long, T> processingRpc = new ConcurrentHashMap<>();

    public void put(Long requestId, T rpcFuture) {
        processingRpc.put(requestId, rpcFuture);
    }

    public T get(Long requestId,T d) {
        T a =  processingRpc.get(requestId);
        if (a==null){
            a = d;
        }
        return a;
    }

    public T get(Long requestId){
        return processingRpc.get(requestId);
    }

    public void remove(Long requestId) {
        processingRpc.remove(requestId);
    }

    public int count() {
        return processingRpc.size();
    }
}
