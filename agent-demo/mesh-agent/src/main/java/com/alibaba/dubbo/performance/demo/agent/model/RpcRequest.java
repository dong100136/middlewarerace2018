package com.alibaba.dubbo.performance.demo.agent.model;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.CompositeByteBuf;
import io.netty.buffer.Unpooled;

import java.util.concurrent.atomic.AtomicLong;

public class RpcRequest {
    private static AtomicLong idHolder = new AtomicLong();
    private static final int HEADER_LENGTH = 12;

    private long id;
    private int length;
    private ByteBuf data;

    public RpcRequest(ByteBuf data) {
        length = data.readableBytes();
        id = idHolder.getAndIncrement();

        byte[] header = new byte[HEADER_LENGTH];
        Bytes.int2bytes(length,header,0);
        Bytes.long2bytes(id,header,4);

        data =Unpooled.compositeBuffer();
        data.writeBytes(header);
        data.writeBytes(data);
        this.data = data;
    }

    public ByteBuf getByteBuf(){
        return data;
    }

    public long getId(){
        return id;
    }
}
