package com.alibaba.dubbo.performance.demo.agent.monitor;

import com.alibaba.dubbo.performance.demo.agent.registry.ChannelMgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class AgentMonitor {
    private static Logger logger = LoggerFactory.getLogger(AgentMonitor.class);
    private static AgentMonitor instance = new AgentMonitor();
    public AtomicLong requestCount = new AtomicLong();
    public AtomicLong finishRequestCount = new AtomicLong();
    public AtomicLong requestProcessingTime = new AtomicLong();
    public AtomicLong decodeTime = new AtomicLong();
    public AtomicLong decodeCount = new AtomicLong();
    public AtomicLong encodeTime = new AtomicLong();
    public AtomicLong encodeCount = new AtomicLong();
    public AtomicLong reqTime = new AtomicLong();
    public AtomicLong respTime = new AtomicLong();
    public AtomicLong remoteProcessingTime = new AtomicLong();
    public Map<String, AtomicInteger> channelsRequestCount = new ConcurrentHashMap<>();
    public Map<String, AtomicLong> channelsProcessTime = new ConcurrentHashMap<>();
    public Map<String, AtomicLong> channelsReqTime = new ConcurrentHashMap<>();
    public Map<String, AtomicLong> channelsRespTime = new ConcurrentHashMap<>();

    private Object lock = new Object();
    private Thread timer = new Thread(() -> {
        while (true) {
            try {
                getInstance().print();
                getInstance().clear();
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    });

    private AgentMonitor() {
        timer.start();
    }

    public static AgentMonitor getInstance() {
        return instance;
    }

    private void print() {
        if (finishRequestCount.get() == 0) return;
        double count = finishRequestCount.get();
        String rs = "";
        rs += "\n**===============agent client monitor===================\n";
        rs += "** finishRequestCount = " + finishRequestCount.get() + '\n';
        rs += "** requestProcessingTime = " + requestProcessingTime.get() + '\n';
        rs += "** requestProcessingAvgTime = " + requestProcessingTime.get() / (finishRequestCount.get() + 0.001) + '\n';
        rs += "** reqTime = " + reqTime.get() + '\n';
        rs += "** reqAvgTime = " + reqTime.get() / (finishRequestCount.get() + 0.001) + '\n';
        rs += "** respTime = " + respTime.get() + '\n';
        rs += "** respAvgTime = " + respTime.get() / (finishRequestCount.get() + 0.001) + '\n';
        rs += "** remoteProcessingTime\t = " + remoteProcessingTime.get() + '\n';
        rs += "** remoteProcessingAvgTime\t = " + remoteProcessingTime.get() / (finishRequestCount.get() + 0.001) + '\n';
        rs += String.format("** %15s %9s %9s %9s %9s %9s %9s\n","channel","count","proc","req","resp","remote","mean");
        for (String key : channelsRequestCount.keySet()) {
            double channelCount = channelsRequestCount.get(key).get();
            long proc = channelsProcessTime.get(key).get();
            long req = channelsReqTime.get(key).get();
            long resp = channelsRespTime.get(key).get();
            long remote = proc - req - resp;
            rs += String.format("** %15s %6.0f, %6.1f ms, %6.1f ms, %6.1f ms, %6.1f ms, %6.1f ms\n",
                    key, channelCount, proc / channelCount, req / channelCount, resp / channelCount, remote / channelCount, ChannelMgr.getMeanProcessingTime(key)
            );
        }
        rs += "**===============agent client monitor===================";
        logger.info(rs);
    }

    private void clear() {
        requestCount.getAndSet(0);
        finishRequestCount.getAndSet(0);
        requestProcessingTime.getAndSet(0);
        reqTime.getAndSet(0);
        respTime.getAndSet(0);
        remoteProcessingTime.getAndSet(0);
        for (String key : channelsRequestCount.keySet()) {
            channelsRequestCount.get(key).set(0);
            channelsProcessTime.get(key).set(0);
            channelsReqTime.get(key).set(0);
            channelsRespTime.get(key).set(0);
        }
    }

    public void addChannelRequest(String hostname, long delta, long reqDelta, long respDelta) {
        if (!channelsRequestCount.containsKey(hostname)) {
            channelsRequestCount.put(hostname, new AtomicInteger());
            channelsProcessTime.put(hostname, new AtomicLong());
            channelsRespTime.put(hostname, new AtomicLong());
            channelsReqTime.put(hostname, new AtomicLong());
        }

        channelsRequestCount.get(hostname).getAndIncrement();
        channelsProcessTime.get(hostname).getAndAdd(delta);
        channelsReqTime.get(hostname).getAndAdd(reqDelta);
        channelsRespTime.get(hostname).getAndAdd(respDelta);
    }


    public void addRequest(long processingDelta, long reqDelta, long respDelta) {
        finishRequestCount.getAndIncrement();
        requestProcessingTime.getAndAdd(processingDelta);
        reqTime.getAndAdd(reqDelta);
        respTime.getAndAdd(respDelta);
        remoteProcessingTime.getAndAdd(processingDelta - reqDelta - respDelta);
    }
}
