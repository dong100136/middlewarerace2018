package com.alibaba.dubbo.performance.demo.agent.registry;

import java.util.*;

public class ChannelMgr {
    private static IRegistry registry = new EtcdRegistry(System.getProperty("etcd.url"));
    private static Map<String, Endpoint> endpoints = new HashMap<>();
    private static Object lock = new Object();
    private static Random random = new Random();
    private static List<String> endpointRandomList = new ArrayList<>();
    private static int MAX_COUNT = 100;
    private static int count = 0;
    private volatile static String lastChannel = "";
    private static int WARMUP_COUNT  = 2000;
    private static int warmupCount = 0;

    private static void init() throws Exception {
        if (null == endpoints || endpoints.size() == 0) {
            synchronized (lock) {
                if (null == endpoints || endpoints.size() == 0) {
                    List<Endpoint> rs = registry.find("com.alibaba.dubbo.performance.demo.provider.IHelloService");
                    for (Endpoint endpoint : rs) {
                        endpoints.put(endpoint.getHost() + ":" + endpoint.getPort(), endpoint);
                        for (int j = 0; j < endpoint.getSize(); j++) {
                            endpointRandomList.add(endpoint.getHost() + ":" + endpoint.getPort());
                        }
                    }

                }
            }
        }
    }

    public static Endpoint getServerByRandom() throws Exception {
        init();

        // 简单的负载均衡，随机取一个
        String endpointIndex = endpointRandomList.get(random.nextInt(endpointRandomList.size()));
        Endpoint endpoint = endpoints.get(endpointIndex);

        return endpoint;
    }

    public static Endpoint getServerByMiniProcessingTime() throws Exception {
        init();

        if (warmupCount<WARMUP_COUNT){
            warmupCount++;
            return getServerByRandom();
        }


//        重置状态
        count++;
        if (count >= MAX_COUNT) {
            for (String channelName : endpoints.keySet()) {
                endpoints.get(channelName).update(50);
            }
            count = 0;
        }

        Endpoint endPoint = null;
        double min = Double.MAX_VALUE;
        for (String channelName : endpoints.keySet()) {
            Endpoint a = endpoints.get(channelName);
            if ((!lastChannel.equals(a.getHost())) && min > a.getMeanProcessingTime()) {
                min = a.getMeanProcessingTime();
                endPoint = a;
            }
        }

        lastChannel = endPoint.getHost();
        return endPoint;
    }

    public static Endpoint getServerByLastActiveTime() throws Exception {
        init();

        long now = System.currentTimeMillis();

        Endpoint endpoint = null;
        long lastActive = -1000000;
        for (String channelName:endpoints.keySet()){
            Endpoint a = endpoints.get(channelName);
            long delta = now-a.lastActiveTime.get();
            if (lastActive<delta){
                lastActive = delta;
                endpoint = a;
            }
        }
        return endpoint;
    }

    public static void update(String channelName, long delta) {
        endpoints.get(channelName).update(delta);
    }

    public static void active(String channelName,long timestamp){
        endpoints.get(channelName).active(timestamp);
    }

    public static double getMeanProcessingTime(String channelName) {
        return endpoints.get(channelName).getMeanProcessingTime();
    }
}
