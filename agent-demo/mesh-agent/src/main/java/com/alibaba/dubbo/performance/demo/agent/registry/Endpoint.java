package com.alibaba.dubbo.performance.demo.agent.registry;

import java.util.concurrent.atomic.AtomicLong;

public class Endpoint {
    private final String host;
    private final int port;
    private final int size;
    private final double updateRate = 0.1;
    private double meanProcessingTime = 50;
    private Object lock = new Object();
    public AtomicLong lastActiveTime = new AtomicLong(System.currentTimeMillis());

    public Endpoint(String host, int port, int size) {
        this.host = host;
        this.port = port;
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String toString() {
        return host + ":" + port;
    }

    public boolean equals(Object o) {
        if (!(o instanceof Endpoint)) {
            return false;
        }
        Endpoint other = (Endpoint) o;
        return other.host.equals(this.host) && other.port == this.port;
    }

    public int hashCode() {
        return host.hashCode() + port;
    }

    public void update(long delta) {
//        去除异常点
        if (delta > 2 * meanProcessingTime) return;

        this.meanProcessingTime = (1 - updateRate) * this.meanProcessingTime + updateRate * delta;
    }

    public void reset() {
        synchronized (lock) {
            this.meanProcessingTime = 50;
        }
    }

    public double getMeanProcessingTime() {
        return meanProcessingTime;
    }

    public void setMeanProcessingTime(double meanProcessingTime) {
        this.meanProcessingTime = meanProcessingTime;
    }

    public void active(long timestamp) {
        lastActiveTime.set(timestamp);
    }
}
