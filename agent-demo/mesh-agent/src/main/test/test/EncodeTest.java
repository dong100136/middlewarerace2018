package test;

import com.alibaba.dubbo.performance.demo.agent.Utils;
import com.alibaba.dubbo.performance.demo.agent.model.proto.RpcRequestProto;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

public class EncodeTest {
    public static void main(String[] args) {
        Random r = new Random();
        long originSize = 0;
        long sSize = 0;

        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            String parameter = RandomStringUtils.random(r.nextInt(1024), true, true);
            RpcRequestProto.RpcRequest.Builder requestBuilder = RpcRequestProto.RpcRequest.newBuilder();
            long requestId = Utils.getRequestId();
            requestBuilder.setRequestId(requestId);
            requestBuilder.setParameter(parameter);
            requestBuilder.setHc(parameter.hashCode());
            requestBuilder.setCreateTime(System.currentTimeMillis());
            requestBuilder.setReqBeginTime(System.currentTimeMillis());
            RpcRequestProto.RpcRequest build = requestBuilder.build();

            originSize += parameter.length();
            sSize+=build.toByteArray().length;
        }
        long end = System.currentTimeMillis();

        System.out.println(end-start);
        System.out.println(originSize/1000);
        System.out.println(sSize/1000);
    }
}
