package test;


import org.apache.commons.lang3.RandomStringUtils;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class QueneTest {
    private ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(50);
    private final int testTime = 10000;
    private AtomicLong finishTask = new AtomicLong();
    private final long start;
    private long end;

    public static void main(String[] args) throws InterruptedException {
        new QueneTest();
    }

    public QueneTest() throws InterruptedException {
        start = System.currentTimeMillis();
        new Thread(new MainThread()).start();
        run();
    }

    public void run() throws InterruptedException {
        for (int i = 0; i < testTime; i++) {
            String r = RandomStringUtils.random(255);
            queue.put(r);
        }
    }

    class MainThread implements Runnable{
//        private ExecutorService executor = Executors.newFixedThreadPool(100);
        private ExecutorService executor = Executors.newFixedThreadPool(10);

        @Override
        public void run() {
            try {
                String s ="";
                while ((s = queue.poll(1, TimeUnit.SECONDS))!=null) {
                    executor.submit(new Task(s));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    class Task implements Runnable{
        private String s = "";

        public Task(String s) {
            this.s = s;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(50);
                System.out.println(finishTask.get()+":"+s.hashCode());
                finishTask.getAndIncrement();

                if (finishTask.get()==testTime){
                    end = System.currentTimeMillis();
                    System.out.println("finish at "+(end-start)+" ms");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
