package test;


import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Random;

public class Test2 {
    Random r = new Random();
    int size1, size2, size3, size4 = 0;

    Kryo kryo = new Kryo();

    {
        kryo.register(String.class);
    }

    static void printSize() {
        for (int i = 0; i < 10000; i++) {

        }
//        System.out.println("stone");
    }

    @Test
    public void test() throws UnsupportedEncodingException {
        String p = RandomStringUtils.random(r.nextInt(1024), true, true);
        size1 += p.getBytes("ascii").length;
        System.out.println(p);
    }

    //    @RepeatedTest(100)
    void test2() {
        String p = RandomStringUtils.random(r.nextInt(1024), true, true);
        size2 += StringCompress.compress(p).length;
    }

    //    @RepeatedTest(100)
    void test3() {
        String p = RandomStringUtils.random(r.nextInt(1024), true, true);
        size3 += StringCompress.compress(p, "ascii").length;
    }

    //    @RepeatedTest(100)
    void test4() {
        String p = RandomStringUtils.random(r.nextInt(1024), true, true);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Output output = new Output(byteArrayOutputStream);

        kryo.writeClassAndObject(output, p);
        output.flush();
        size4 += byteArrayOutputStream.toByteArray().length;
    }

}
