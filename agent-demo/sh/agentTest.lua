done = function(summary, latency, requests)

    io.write("--------------------------\n")
    local durations = summary.duration / 1000000 -- 执行时间，单位是秒
    local errors = summary.errors.status -- http status不是200，300开头的
    local requests = summary.requests -- 总的请求数
    local valid = requests - errors -- 有效请求数=总请求数-error请求数


    io.write("Durations:       " .. string.format("%.2f", durations) .. "s" .. "\n")
    io.write("Requests:        " .. summary.requests .. "\n")
    io.write("Avg RT:          " .. string.format("%.2f", latency.mean / 1000) .. "ms" .. "\n")
    io.write("Max RT:          " .. (latency.max / 1000) .. "ms" .. "\n")
    io.write("Min RT:          " .. (latency.min / 1000) .. "ms" .. "\n")
    io.write("Error requests:  " .. errors .. "\n")
    io.write("Valid requests:  " .. valid .. "\n")
    io.write("QPS:             " .. string.format("%.2f", valid / durations) .. "\n")
    io.write("--------------------------\n")
end

response = function(status, headers, body)
    if not token and status ~= 200 then
        io.write(wrk.format(headers, body))
    end
end

--wrk.host  = "localhost"
--wrk.port = 20000
--wrk.path = 'invoke'
wrk.method = 'POST'
wrk.body = 'interface=com.alibaba.dubbo.performance.demo.provider.IHelloService&method=hash&parameterTypesString=Ljava/lang/String;&parameter=tjyrAzoY3eQFbRx3utuPPG6n6zdxlUuWY4IuYtA09aX6t8Aa9iWu4F4VTJwfDljkxGavZjQc8mLRLS3wtXhw3HQJzyZGzXOeHqnigcNjZj3cwZsl8i1vKyDR65JhREbrxDipp38StFgRFduEMumNeqqhfygTM4gaHuUMmX93ZS5bOi7clRh7JQIA0Ev1hV4qhE3XQkdG9Ezaq2GVUSG1mpFoDJCEZrTFyn4MUxf5fHWJQqDZahkxULguSC9JImJyg6C42pBP1sUg6tZvPT4qHIuDtS0cPgtRMFHjfyYxqZ57QuYRAvXTwDynNdVSPVeTFVZCVrm8uNNkrzDOihP8yhhTjjJ5yeTvIWcecyzSuvU3jkoWHppuzIlivdqA7u3nxkOIv4Bu6xYmvDQFX41vx1ePhQThO85KImMWvLjtF1YZ66CqMKjMiS2yKlqKXfaX7yZiEDy1xKkvXQaJWzIPzyL4uwqTfjOlGCZrZ98spxNygLlbS4B6DBlY2RMNCB81mfYTp5bUwK3wYBsKOWYZzJiEe94m66mCby7uHVfODu6a5J06epzdS1dRChguoCumab1FFxwZp04yHwmWHXtUDGz46NAYo57oAlNEcTNwlXd0RG5TvLwAubNcUfP6b5X7JFVYDWpD5FWQr0NUCbfp3Lmkx6tJOw93O5HQLPkgCyM5LzQR9Zo0KiAOtMOLqY6uhvrNJfwSjwYadCgHt6MNPTtzK9ctiLLXB4ZN6EIL6zgqK3ezAlu6XFmYKhpt2hIAq1UtJ9naWphOszigtb0xRVYWGqZ4P9Evyu86O4yLC7m069ly9wtiX2NMSBjZt3CpGMvQC746gcMd1fzrSHHvNLBx71mUeqw2LX6G41HCdxMN4ThfvtQeeVCNC9M7YeIfemutFrS7UXvZd9PHfVLeYCgb5PwPyecCjnzyC0FFnkKdRTBNECIQA'

request = function()
    local body = wrk.format(nil, nil, nil, nil)
    return body
end
