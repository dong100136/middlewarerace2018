#!/usr/bin/env bash

wrk -t2 -c64 -d5s -T5 \
--script=wrk.lua \
--latency http://localhost:18087/invoke

wrk -t2 -c128 -d5s -T5 \
--script=wrk.lua \
--latency http://localhost:18087/invoke

wrk -t2 -c256 -d5s -T5 \
--script=wrk.lua \
--latency http://localhost:18087/invoke