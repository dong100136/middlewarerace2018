#!/usr/bin/env bash

cd ../mesh-agent && set -ex && mvn clean package
docker build .. -t  registry.cn-hangzhou.aliyuncs.com/sunshangwei/tianchi:$1
docker push  registry.cn-hangzhou.aliyuncs.com/sunshangwei/tianchi:$1

#docker build .. -t  10.106.129.240:5000/sunshangwei/tianchi:$1
#docker push  10.106.129.240:5000/sunshangwei/tianchi:$1
