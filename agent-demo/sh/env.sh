#!/usr/bin/env bash

# environment
provider_jar=../mesh-provider-1.0-SNAPSHOT.jar
consumer_jar=../mesh-consumer-1.0-SNAPSHOT.jar
agent_jar=../mesh-agent/target/mesh-agent-1.0-SNAPSHOT.jar

LOG_HOME=../log
PROVIDER_HOME=$LOG_HOME/provider
CONSUMER_HOME=$LOG_HOME/consumer
ETCD_HOME=$LOG_HOME/etcd

# start etcd
ETCD_URL=http://127.0.0.1:2379