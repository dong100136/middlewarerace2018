#!/usr/bin/env bash
# 不到万不得已，不要使用这个

ps -ef | grep 'dubbo' | cut -d ' ' -f 4 | xargs kill

ps -ef | grep /opt/etcd/etcd | cut -d ' ' -f 4 | xargs kill
