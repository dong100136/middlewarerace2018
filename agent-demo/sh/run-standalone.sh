#!/usr/bin/env bash

source ./env.sh

echo "start compile agent"
cd ..
mvn clean package
cd sh
echo "finish compile agent"

if [ -e $LOG_HOME/.lock ];then
    echo "stop last process"
    sh ./stop-standalone.sh
fi

echo "clean all log"
find ././../log/ -name "*.log" | xargs rm -f

touch $LOG_HOME/.lock

if [ ! -e $ETCD_HOME/run.pid ];then
    echo "start etcd on $ETCD_URL"
    nohup /opt/etcd/etcd \
          --listen-client-urls $ETCD_URL \
          --advertise-client-urls $ETCD_URL \
          --data-dir $ETCD_HOME/data > $ETCD_HOME/etcd.log 2>&1 &
    echo $! > $ETCD_HOME/run.pid
else
    echo "already run etcd"
fi

# start provider
if [ ! -e ${PROVIDER_HOME}/provider.pid ];then
    echo "start provider on http://localhost:8085"
    nohup java -jar \
        -Xms1G \
        -Xmx1G \
        -Ddubbo.protocol.port=20889 \
        -Ddubbo.application.qos.enable=false \
        -Dlogs.dir=${PROVIDER_HOME} \
        ${provider_jar} \
        > /dev/null 2>&1 &
    echo $! > ${PROVIDER_HOME}/provider.pid
else
    echo "already start provider"
fi

if [ ! -e $PROVIDER_HOME/provider-agent1.pid ];then
    echo "start provider agent on http://localhost:30000"
    nohup java -jar \
           -Xms512M \
           -Xmx512M \
           -Dtype=provider \
           -Dserver.port=30000 \
           -Ddubbo.protocol.port=20889 \
           -Detcd.url=$ETCD_URL \
           -Dlogs.dir=$PROVIDER_HOME \
           $agent_jar > /dev/null 2>&1 &

    echo $! > $PROVIDER_HOME/provider-agent1.pid
else
    echo "already start provider agent"
fi

if [ ! -e $PROVIDER_HOME/provider-agent2.pid ];then
    echo "start provider agent on http://localhost:30001"
    nohup java -jar \
           -Xms512M \
           -Xmx512M \
           -Dtype=provider \
           -Dserver.port=30001 \
           -Ddubbo.protocol.port=20889 \
           -Detcd.url=$ETCD_URL \
           -Dlogs.dir=$PROVIDER_HOME \
           $agent_jar > /dev/null 2>&1 &

    echo $! > $PROVIDER_HOME/provider-agent2.pid
else
    echo "already start provider agent"
fi

# start consumer
if [ ! -e $CONSUMER_HOME/consumer.pid ];then
    echo "start consumer on http://localhost:18087"
    nohup java -jar \
        -Xms1G \
        -Xmx1G \
        -Ddubbo.protocol.port=20889 \
        -Ddubbo.application.qos.enable=false \
        -Dlogs.dir=$CONSUMER_HOME \
        $consumer_jar \
        > /dev/null 2>&1 &
    echo $! > $CONSUMER_HOME/consumer.pid
else
    echo "already start consumer"
fi

#if [ ! -e $CONSUMER_HOME/consumer-agent.pid ];then
#    echo "start consumer agent on http://localhost:20000"
#    nohup java -jar \
#       -Xms1536M \
#       -Xmx1536M \
#       -Dtype=consumer \
#       -Dserver.port=20000\
#       -Detcd.url=$ETCD_URL \
#       -Dlogs.dir=$CONSUMER_HOME \
#       $agent_jar > /dev/null 2>&1 &
#    echo $! > $CONSUMER_HOME/consumer-agent.pid
#else
#    echo "already start consumer agent"
#fi

echo "visit http://localhost:18087/invoke"


