#!/usr/bin/env bash

source ./env.sh

# stop etcd
PID=$(cat $ETCD_HOME/run.pid)
kill -9 $PID
rm -f $ETCD_HOME/run.pid

# stop provider
PID=$(cat $PROVIDER_HOME/provider.pid)
kill -9 $PID
rm -f $PROVIDER_HOME/provider.pid

PID=$(cat $PROVIDER_HOME/provider-agent1.pid)
kill -9 $PID
rm -f $PROVIDER_HOME/provider-agent1.pid

PID=$(cat $PROVIDER_HOME/provider-agent2.pid)
kill -9 $PID
rm -f $PROVIDER_HOME/provider-agent2.pid

# stop consumer
PID=$(cat $CONSUMER_HOME/consumer.pid)
kill -9 $PID
rm -f $CONSUMER_HOME/consumer.pid

PID=$(cat $CONSUMER_HOME/consumer-agent.pid)
kill -9 $PID
rm -f $CONSUMER_HOME/consumer-agent.pid


rm -f $LOG_HOME/.lock