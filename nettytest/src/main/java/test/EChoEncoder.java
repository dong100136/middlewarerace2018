package test;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class EChoEncoder extends MessageToByteEncoder<Invocation>{

    protected void encode(ChannelHandlerContext channelHandlerContext, Invocation invocation, ByteBuf byteBuf) throws Exception {
        byte[] msg = new Gson().toJson(invocation).getBytes();
        byteBuf.writeBytes(msg);
    }
}
