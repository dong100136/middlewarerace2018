package test;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.util.CharsetUtil;

@ChannelHandler.Sharable
public class EchoClientHandler extends SimpleChannelInboundHandler<byte[]> {
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, byte[] bytes) throws Exception {
        String msg = new String(bytes);
        Invocation invocation = new Gson().fromJson(msg,Invocation.class);
        System.out.println("client receive: "+invocation.toString());
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Invocation invocation = new Invocation();
        invocation.setName("stone");
        invocation.setAge(10);

        String msgStr = new Gson().toJson(invocation);
        System.out.println(msgStr);
        final byte[] msg = msgStr.getBytes();

        for (byte b:msg){
            System.out.println(b);
        }

        final ByteBuf bytes = ctx.alloc().buffer(1024);
        int savedIndex = bytes.writerIndex();
        bytes.writerIndex(savedIndex);
        bytes.writeBytes(msg);
        bytes.writerIndex(savedIndex+msg.length);

        ctx.writeAndFlush(bytes).addListener(new ChannelFutureListener() {
            public void operationComplete(ChannelFuture channelFuture) throws Exception {
                System.out.println("send data finish "+msg.length);
            }
        });
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
