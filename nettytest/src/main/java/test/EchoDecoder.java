package test;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class EchoDecoder extends ByteToMessageDecoder {
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        byte[] msg = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(msg);

        String msgStr = new String(msg);
        Invocation invocation = new Gson().fromJson(msgStr,Invocation.class);
        list.add(invocation);
    }
}
