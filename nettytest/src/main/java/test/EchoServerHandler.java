package test;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;

@ChannelHandler.Sharable
public class EchoServerHandler extends ChannelInboundHandlerAdapter{
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("stone");
        ByteBuf in = (ByteBuf) msg;
        byte[] bytes = new byte[in.readableBytes()];
        in.readBytes(bytes);
        System.out.println("length "+bytes.length+" "+bytes);

        for (byte b:bytes){
            System.out.println(b);
        }

        String msgStr = new String(bytes);
        System.out.println(msgStr);
        Invocation invocation = new Gson().fromJson(msgStr,Invocation.class);
        System.out.println("client receive: "+invocation.toString());
    }
    //    protected void channelRead0(ChannelHandlerContext channelHandlerContext, byte[] bytes) throws Exception {
//        String msg = new String(bytes);
//        Invocation invocation = new Gson().fromJson(msg,Invocation.class);
//        System.out.println("client receive: "+invocation.toString());
//        channelHandlerContext.writeAndFlush(bytes);
//    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }
}
